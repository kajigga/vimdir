set nocompatible
filetype off
execute pathogen#infect()

filetype indent plugin on
syntax on
set nu
set nowrap

if has("gui_running")
  if has("gui_gtk2")
    set guifont=Inconsolata\ 12
  elseif has("gui_macvim")
    set guifont=Menlo\ Regular:h14
    set guicursor=a:hor15-Cursor
  elseif has("gui_win32")
    "set guifont=Consolas:h11:cANSI
    set guifont=Source_Code_Pro:h11:cANSI
  endif
endif
set expandtab
set autoindent
set tabstop=2
set shiftwidth=2
set softtabstop=2
set cursorcolumn
set cursorline

" textwidth = 85
set tw=85


" Solarized Theme
set bg=dark
colorscheme solarized

autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType html,xml,xsl runtime scripts\closetag.vim

" Nerdtree Stuff
let NERDTreeIgnore = ['\.pyc$','\.zip', 'icutils\.egg-info']

" Make .dust files be treated as .html
"au BufReadPost *.dust set syntax=html
"
let g:UltiSnipsEditSplit="vertical"


" vim-closetag options
let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.xml,*.tpl""

au FileType xml setlocal equalprg=xmllint\ --format\ --recover\ -\ 2>/dev/null

" set backupdir=$HOME/.vimbackup,$HOME/.vimtmp
" set directory=$HOME/.vimbackup,$HOME/.vimtmp

vmap <C-c> :w !pbcopy<CR><CR>
